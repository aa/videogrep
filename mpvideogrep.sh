#! /usr/bin/env bash

# Usage:
#
#    ./mpvideogrep.sh movie sub regex
#
# Example:
#
#    ./mpvideogrep.sh movie.avi movie.sub "\b(wo)?m[ae]n\b"

vid=$1
sub=$2
term=$3

framerate=$(ffprobe -i "${vid}" 2>&1 | sed -n "s/.*, \(.*\) fp.*/\1/p")

grep -E -a -i "${term}" "${sub}" | \
sed -e 's/^{\([[:digit:]]*\)}{\([[:digit:]]*\)}.*/\1 \2/' | \
awk '{ print "'${vid}'," ( $1 / '${framerate}' ) "," ( $2 / '${framerate}' ) - ( $1 / '${framerate}' ) }' | \
sed -e '1s/^/# mpv EDL v0\n/' \
> videogrep.edl

grep -E -a -i "${term}" "${sub}" | \
awk -F '[{}]' 'match($0,/[{][0-9]+[}][{][0-9]+[}]/) { tmp = old + $4 - $2; printf("{%d}{%d}%s\n", old, tmp, substr($0, RLENGTH+1)); old = tmp  }' \
> videogrep.sub

mpv --sub-file=videogrep.sub --fs videogrep.edl
